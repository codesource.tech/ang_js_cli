# componentGen.py
import next, os, globalsList


def genComponent(name, ty):
    if ty == "component":
        newComponentDir = os.path.join(globalsList.componentsDir, name)
        os.makedirs(newComponentDir)
        generateIndex("./components", name)
        print("Creating a new ", ty, " named: ", name)
        yN = yesNo(
            "Do you want to inject dependencies/services into your componenet? e.g. $http, ..."
        )
        print(yN)
        if yN == "yes":
            deps = input("Type your dependencies seperated by commas(no spaces!):")
        else:
            deps = ""
        depsArr1 = deps.split(",")
        deps1 = '", "'.join(depsArr1)
        if yN == "yes":
            depsArr = '"' + deps1 + '"'
            comma = ","
        else:
            comma = ""
            depsArr = ""
        componentJsName = name + ".js"
        scriptsPath = os.path.join(newComponentDir + "\\" + componentJsName)
        js = open(scriptsPath, "w")
        jsFile = """(function(){{
        "use strict"; 
        var module = angular.module("{}"); 
        module.controller("{}Ctrl", [{}{} function ({}) {{
            var model = this; 
            model.$onInit = function(){{
            }};
            model.$onChanges = function(){{
            }};
            model.$onDestroy = function(){{
            }};
        }}]);
        module.component("{}", {{
            controller: "{}Ctrl", 
            controllerAs: "model", 
            templateUrl: "./components/{}/{}.html"
            }});
        }}());""".format(
            projectName, name, depsArr, comma, deps, name, name, name, name
        )
        js.write(jsFile)
        js.close()
        componentHtmlName = name + ".html"
        templatePath = os.path.join(newComponentDir + "\\" + componentHtmlName)
        tempalte = open(templatePath, "w")
        templateFile = """<!-- This is where you put your template HTML --> 
        <h1>This is the {} component. Put something here! </h1> """.format(
            name
        )
        tempalte.write(templateFile)
        tempalte.close()
        next.next()
