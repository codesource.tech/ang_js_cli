#! /bin/bash

if [ ! -f $PWD/package.json ]; then
    echo "Error, package.json not found! No project found in current working directory. Please run ang_gen.sh first. Make sure you are in the right directory!"
    exit 1
fi

function join() {
    for i in "$@"; do
        #echo -n "$i""', '"
        newArr=$newArr"$i""', '"
    done
    echo
}
echo 'What do you want to name your component?'
read componentName
read -n1 -p "Do you want to inject dependencies into the controller? Type (y) for yes or (n) for no" doit 
case $doit in  
    y|Y) echo -e "\\nOkay! Please type your dependedcies below seperated by a comma without spaces."
        read depsArr
        #echo $depsArr
        arrIN=(${depsArr//,/ })
        #arrayString=$( IFS=$"', "  ; echo "${arrIN[*]}" )
        #echo $arrayString
        #for i in "${arrIN[@]}"
        #do  
        #   echo $i"', '"
        #done
        join ${arrIN[@]}
        newArr2="'"$newArr
        
        newArr3=${newArr2: : -2}
        echo $newArr3
;; 
    n|N) echo -e "\\nno, Okay, moving on... " ;; 
  *) echo 'You need to type y for yes or n for no!' 
    exit 1
;; 
esac
mkdir $PWD/app/components/$componentName
# generate javascript for component
touch $PWD/app/components/$componentName/$componentName.js
echo '/* Angular JS component '$componentName' */' >> $PWD/app/components/$componentName/$componentName.js
echo '(function () {' >> $PWD/app/components/$componentName/$componentName.js
echo '    "use strict";' >> $PWD/app/components/$componentName/$componentName.js
echo '' >> $PWD/app/components/$componentName/$componentName.js
echo 'var module = angular.module("app");' >> $PWD/app/components/$componentName/$componentName.js
echo 'module.component("'$componentName'", {' >> $PWD/app/components/$componentName/$componentName.js
echo '        templateUrl:"'$componentName'/'$componentName'.html", ' >> $PWD/app/components/$componentName/$componentName.js
echo '        controller: "'$componentName'Ctrl",' >> $PWD/app/components/$componentName/$componentName.js
echo '        controllerAs: "model"' >> $PWD/app/components/$componentName/$componentName.js
echo '   });' >> $PWD/app/components/$componentName/$componentName.js

echo '        ' >> $PWD/app/components/$componentName/$componentName.js
echo 'module.controller("'$componentName'Ctrl", ['$newArr3 ' function ('$depsArr') {' >> $PWD/app/components/$componentName/$componentName.js

echo '      var model = this;' >> $PWD/app/components/$componentName/$componentName.js
echo '   }]);' >> $PWD/app/components/$componentName/$componentName.js

echo '}());' >> $PWD/app/components/$componentName/$componentName.js

# inject component into main index.html
 sed '/^<!-- Linked JS -->$/,/^<!-- END Linked JS -->$/{H;//{x;/extension="memcache.so"/{p;d};/;;;\n/{s//&extension="memcache.so"\n/p}};d}' app/index.html


# Generate html template for component
touch $PWD/app/components/$componentName/$componentName.template.html
echo '/* Angular JS component '$componentName' template file */' >> $PWD/app/components/$componentName/$componentName.template.html 
echo '/* Your HTML template code goes here */' >> $PWD/app/components/$componentName/$componentName.template.html 



